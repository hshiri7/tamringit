<?php

require_once 'connect.php';

require_once 'header.php';


?>
<div class="container">
	<?php

	if (isset($_POST['addnew'])) {

		if (empty($_POST['Username']) || empty($_POST['Email']) || empty($_POST['password']) || empty($_POST['NationalCode'])) {
			echo "Please fillout all required fields";
		} else {
			$Username  = $_POST['Username'];
			$Email 	= $_POST['Email'];
			$password 	= $_POST['password'];
			$NationalCode  	= $_POST['NationalCode'];
			$sql = "INSERT INTO users(Username,Email,password,NationalCode) 
		    VALUES('$Username','$Email','$password','$NationalCode')";

			if ($con->query($sql) === TRUE) {
				echo "<div class='alert alert-success'>Successfully added new user</div>";
			} else {
				echo "<div class='alert alert-danger'>Error: There was an error while adding new user</div>";
			}
		}
	}
	?>
	<div class="row justify-content-center">
		<div class="col-md-6 col-md-offset-3">
			<div class="box">
				<h3 class="text-center">Add New User</h3>
				<form action="" method="POST">
					<label for="Username">Username</label>
					<input type="text" id="Username" name="Username" class="form-control"><br>
					<label for="Email">Email</label>
					<input type="email" name="Email" id="Email" class="form-control"><br>
					<label for="NationalCode">NationalCode</label>
					<input type="text" name="NationalCode" id="NationalCode" class="form-control"><br>
					<br>
					<label for="password">Password</label>
					<input type="password" name="password" id="password" class="form-control"><br>
					<br>
					<input type="submit" name="addnew" class="btn btn-success" value="Add New">
				</form>
			</div>
		</div>
	</div>
</div>

<?php
