<?php

require_once 'connect.php';

require_once 'header.php';

?>
<div class="container">
	<?php

	if (isset($_POST['update'])) {

		if (
			empty($_POST['Username']) || empty($_POST['Email']) ||
			empty($_POST['password']) || empty($_POST['NationalCode'])
		) {
			echo "Please fillout all required fields";
		} else {
			$Username  = $_POST['Username'];
			$Email 	= $_POST['Email'];
			$password 	= $_POST['password'];
			$NationalCode  	= $_POST['NationalCode'];
			$sql = "UPDATE users SET Username='{$Username}', Email = '{$Email}',
						password = '{$password}', NationalCode = '{$NationalCode}' 
						WHERE user_id=" . $_POST['userid'];

			if ($con->query($sql) === TRUE) {
				echo "<div class='alert alert-success'>Successfully updated  user</div>";
			} else {
				echo "<div class='alert alert-danger'>Error: There was an error while updating user info</div>";
			}
		}
	}
	$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
	$sql = "SELECT * FROM users WHERE user_id={$id}";
	$result = $con->query($sql);

	if ($result->num_rows < 1) {
		header('Location: index.php');
		exit;
	}
	$row = $result->fetch_assoc();
	?>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="box">
				<h3><i class="glyphicon glyphicon-plus"></i>&nbsp;MODIFY User</h3>
				<form action="" method="POST">
					<input type="hidden" value="<?php echo $row['user_id']; ?>" name="userid">
					<label for="Username">Username</label>
					<input type="text" id="Username" name="Username" value="<?php echo $row['Username']; ?>" class="form-control"><br>
					<label for="Email">Email</label>
					<input type="text" name="Email" id="Email" value="<?php echo $row['Email']; ?>" class="form-control"><br>
					<label for="password">password</label>
					<input type="password" name="password" class="form-control" value="?php echo $row['password']; ?"></input><br>
					<label for="NationalCode">NationalCode</label>
					<input type="text" name="NationalCode" id="NationalCode" value="<?php echo $row['NationalCode']; ?>" class="form-control"><br>
					<br>
					<input type="submit" name="update" class="btn btn-success" value="Update">
				</form>
			</div>
		</div>
	</div>
</div>

<?php
