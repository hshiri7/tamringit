<?php
session_start();
?>

<!DOCTYPE html>
<html>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tamrin3</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  <script>
    $(document).ready(function() {
      // disable submit


      $('#login').validate({

        rules: {
          username: {
            required: true,
            minlength: 3,
          },
          family: {
            required: true,
          },

          password: {
            required: true,

          },


        },
        messages: {
          username: 'This field is required and more than 3 char',

          password: {
            minlength: 'Password must be at least 6 characters long'
          }
        },
        submitHandler: function(form) {
          form.submit();
        }
      });

    });
  </script>
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
      <ul class="navbar-nav  ">
        
        <!-- Menu apear when loged in -->
        <?php if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == true): ?>
        <li class="nav-item">
          <a class="nav-link" href="insert.php">Insert User</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="users.php">users</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Logout</a>
        </li>
        <?php else: ?>
        <li class="nav-item ">
          <a class="nav-link" href="index.php">Login </a>
        </li>
        <?php endif; ?>

      </ul>
    </div>
  </nav>